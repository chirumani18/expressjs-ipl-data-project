const express = require("express")
const path = require("path")
const app = express()
let PORT = process.env.PORT || 5000

const problem1 = require('../server/1-matches-per-year.cjs')
const problem2 = require('../server/2-matches-won-per-team-per-year.cjs')
const problem3 = require('../server/3-extra-runs-conceded-per-team-2016.cjs')
const problem4 = require('../server/4-top-10-economical-bowlers-2015.cjs')
const problem5 = require('../server/5-won-both-matches-and-toss.cjs')
const problem6 = require('../server/6-highest-player-of-the-match-award.cjs')
const problem7 = require('../server/7-strike-rate-of-batsman-each-season.cjs')
const problem8 = require('../server/8-highest-number-times-dismissed-player.cjs')
const problem9 = require('../server/9-bowler-with-best-eco-super-over.cjs')
app.get('/problem1', (request, response) => {
    response.setHeader("400", {
        'Content-type': 'text/plain'
    })
    response.send(problem1)
    response.end()
})

app.get('/problem2', (request, response) => {
    response.setHeader("400", {
        'Content-type': 'text/plain'
    })
    response.send(problem2)
    response.end()
})

app.get('/problem3', (request, response) => {
    response.setHeader("400", {
        'Content-type': 'text/plain'
    })
    response.send(problem3)
    response.end()
})

app.get('/problem4', (request, response) => {
    const data = problem4()
    response.setHeader("400", {
        'Content-type': 'text/plain'
    })

    response.send(data)
    response.end()
})

app.get('/problem5', (request, response) => {
    const data = problem5()
    response.setHeader("400", {
        'Content-type': 'text/plain'
    })

    response.send(data)
    response.end()
})

app.get('/problem6', (request, response) => {
    const data = problem6()
    response.setHeader("400", {
        'Content-type': 'text/plain'
    })

    response.send(data)
    response.end()
})

app.get('/problem7', (request, response) => {
    const data = problem7()
    response.setHeader("400", {
        'Content-type': 'text/plain'
    })

    response.send(data)
    response.end()
})

app.get('/problem8', (request, response) => {
    const data = problem8()
    response.setHeader("400", {
        'Content-type': 'text/plain'
    })

    response.send(data)
    response.end()
})

app.get('/problem9', (request, response) => {
    const data = problem9
    response.setHeader("400", {
        'Content-type': 'text/plain'
    })
    response.send(data)
    response.end()
})


app.listen(PORT, () => {
    console.log(`Server is running in ${PORT}`)
})