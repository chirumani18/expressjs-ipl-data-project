const fs = require('fs')
const path = require("path")
const papaparse = require('papaparse')

const matchesFilePath = path.join(__dirname, "..", 'data/matches.csv')
const deliveriesFilePath = path.join(__dirname, "..", 'data/deliveries.csv')

let matchesData = fs.readFileSync(matchesFilePath, 'utf-8')
let deliveriesData = fs.readFileSync(deliveriesFilePath, 'utf-8')

const matchesParsedData = papaparse.parse(matchesData, {
    header: true,
})

const deliveriesParsedData = papaparse.parse(deliveriesData, {
    header: true,
})

function extraRunsConcededPerTeamIn2016(matchesData, deliveriesData) {
    const matches2016 = matchesData.data.filter((match) => match['season'] == '2016')
    const matches2016Ids = matches2016.map((match) => match['id'])
    matches2016Ids.sort()
    const minId = matches2016Ids[0]
    const maxId = matches2016Ids[matches2016Ids.length - 1]
    const extraRunsConcededPerTeam = deliveriesData.data.reduce((accumulator, match) => {
        const matchId = parseInt(match['match_id'])
        if (matchId >= minId && matchId <= maxId) {
            const team = match['bowling_team']
            const extraRuns = parseInt(match['extra_runs'])
            if (accumulator[team] == undefined) {
                accumulator[team] = extraRuns
            } else {
                accumulator[team] += extraRuns
            }
        }
        return accumulator
    }, {})
    return extraRunsConcededPerTeam
}

module.exports = extraRunsConcededPerTeamIn2016(matchesParsedData, deliveriesParsedData)