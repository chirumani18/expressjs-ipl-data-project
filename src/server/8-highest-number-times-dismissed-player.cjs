const fs = require('fs')
const path = require("path")
const papaparse = require('papaparse')

const deliveriesFilePath = path.join(__dirname, "..", 'data/deliveries.csv')

let deliveriesData = fs.readFileSync(deliveriesFilePath, 'utf-8')

const parsedData = papaparse.parse(deliveriesData, {
    header: true,
})

function highestDismissalsBatsman() {
    const dismissals = parsedData.data.reduce((accumulator, wicket) => {
        const batsman = wicket['player_dismissed']
        const bowler = wicket['bowler']
        const wicketType = wicket['dismissal_kind']

        if (wicketType !== "run out" && batsman !== "") {
            if (accumulator[batsman] == undefined) {
                accumulator[batsman] = {}
            }

            if (accumulator[batsman][bowler] == undefined) {
                accumulator[batsman][bowler] = 1
            } else {
                accumulator[batsman][bowler]++
            }
        }
        return accumulator
    }, {})
    const highestNumberOfDismissals = Object.entries(dismissals).reduce((accumulator, [batsman, stats]) => {
        const bowlerWithMaximumDismissals = Object.entries(stats).reduce((maximumDismissals, [bowler, count]) => {
            if (count > maximumDismissals.count) {
                return { bowler, count }
            } else {
                return maximumDismissals
            }
        }, { bowler: '', count: 0 });
        accumulator[batsman] = { bowler: bowlerWithMaximumDismissals.bowler, count: bowlerWithMaximumDismissals.count }
        return accumulator
    }, {});

    const sortDismissals = Object.entries(highestNumberOfDismissals).sort((before, present) => present[1].count - before[1].count)
    return sortDismissals[0]
}

module.exports = highestDismissalsBatsman