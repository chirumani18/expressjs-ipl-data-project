const fs = require('fs')
const path = require("path")
const papaparse = require('papaparse')

const matchesFilePath = path.join(__dirname, "..", 'data/matches.csv')
const deliveriesFilePath = path.join(__dirname, "..", 'data/deliveries.csv')

let matchesData = fs.readFileSync(matchesFilePath, 'utf-8')
let deliveriesData = fs.readFileSync(deliveriesFilePath, 'utf-8')

const matchesParsedData = papaparse.parse(matchesData, {
    header: true,
})

const deliveriesParsedData = papaparse.parse(deliveriesData, {
    header: true,
})

function top10EconomicalBowlersIn2015() {
    const allMatchesIn2015 = matchesParsedData.data.filter((match) => match['season'] == "2015")
    const idsList = allMatchesIn2015.map((match) => match['id']).sort()
    const minimumId = idsList[0]
    const maximumId = idsList[idsList.length - 1]
    const bowlersData = deliveriesParsedData.data.reduce((accumulator, delivery) => {
        const matchId = parseInt(delivery['match_id'])
        if (matchId >= parseInt(minimumId) && matchId <= parseInt(maximumId)) {
            const bowler = delivery['bowler']
            if (accumulator[bowler] == undefined) {
                accumulator[bowler] = { bowlerRuns: 0, bowlerBalls: 0 }
            }

            if (delivery['wide_runs'] === "0" && delivery['noball_runs'] === "0") {
                accumulator[bowler]['bowlerRuns'] += (parseInt(delivery['total_runs']) - parseInt(delivery['extra_runs']))
                accumulator[bowler]['bowlerBalls']++
            }
        }
        return accumulator
    }, {})
    // console.log(Object.keys(bowlersData))
    let bowlers = Object.keys(bowlersData).reduce((accumulator, bowler) => {
        const bowlerEconomy = (bowlersData[bowler]['bowlerRuns'] / bowlersData[bowler]['bowlerBalls']) * 6

        accumulator[bowler] = {
            ...bowlersData[bowler],
            economy: bowlerEconomy
        }
        return accumulator
    }, {})
    // console.log(Object.entries(bowlers))
    const sortedEconomyBowlers = Object.entries(bowlers).sort(
        (before, present) => before[1].economy - present[1].economy
    )
    // console.log(sortedEconomyBowlers)
    const top10EconomyBowlers = sortedEconomyBowlers.slice(0, 10).map((stats) => ({
        bowlerName: stats[0],
        bowlerEconomy: stats[1].economy,
    }))

    return top10EconomyBowlers
}



module.exports = top10EconomicalBowlersIn2015