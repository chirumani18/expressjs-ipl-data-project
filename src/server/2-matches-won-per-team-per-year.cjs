const fs = require("fs")
const path = require("path")
const papaparse = require("papaparse")

const matchesFilePath = path.join(__dirname, "..", 'data/matches.csv')

let matchesData = fs.readFileSync(matchesFilePath, 'utf-8')

const parsedData = papaparse.parse(matchesData, {
    header: true,
})

function matchesWonPerTeamPerYear(parsedData) {
    const allYears = parsedData.data.map((match) => {
        return match.season
    })
    let matchesWonPerYear = allYears.reduce((accumulator, year) => {
        let matchesWonPerTeam = parsedData.data.reduce((teamAccumulator, match) => {
            if (match.season == year) {
                let winner = match.winner
                if (teamAccumulator[winner] == undefined) {
                    teamAccumulator[winner] = 1
                }
                else {
                    teamAccumulator[winner] += 1
                }
            }
            return teamAccumulator
        }, {})
        accumulator[year] = matchesWonPerTeam
        return accumulator
    }, {})
    return matchesWonPerYear
}

module.exports = matchesWonPerTeamPerYear(parsedData)