const fs = require('fs')
const path = require("path")
const papaparse = require('papaparse')

const matchesFilePath = path.join(__dirname, "..", 'data/matches.csv')

let matchesData = fs.readFileSync(matchesFilePath, 'utf-8')

const parsedData = papaparse.parse(matchesData, {
    header: true,
})

function playerOfTheMatchForEveryYear() {
    let years = parsedData.data.reduce((accumulator, match) => {
        const season = match.season

        if (!accumulator.includes(season) && season != undefined) {
            accumulator.push(season)
        }

        return accumulator

    }, [])

    let MostManOfTheMatch = years.reduce((accumulator, year) => {
        let manOfTheMatch = parsedData.data.reduce((teamAccumulator, match) => {
            if (match['season'] == year) {
                let playerOfTheMatch = match['player_of_match']
                if (teamAccumulator[playerOfTheMatch] == undefined) {
                    teamAccumulator[playerOfTheMatch] = 1
                }
                else {
                    teamAccumulator[playerOfTheMatch] += 1
                }
            }
            return teamAccumulator
        }, {})

        let resultArray = Object.entries(manOfTheMatch)

        resultArray.sort((before, present) => present[1] - before[1])
        const sortedResultArray = Object.fromEntries(resultArray)
        console.log(Object.keys(sortedResultArray))
        accumulator[year] = Object.keys(sortedResultArray)[0]

        return accumulator
    }, {})
    return MostManOfTheMatch
}

module.exports = playerOfTheMatchForEveryYear