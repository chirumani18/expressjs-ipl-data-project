const fs = require('fs')
const path = require("path")
const papaparse = require('papaparse')

const matchesFilePath = path.join(__dirname, "..", 'data/matches.csv')
const deliveriesFilePath = path.join(__dirname, "..", 'data/deliveries.csv')

let matchesData = fs.readFileSync(matchesFilePath, 'utf-8')
let deliveriesData = fs.readFileSync(deliveriesFilePath, 'utf-8')

const matchesParsedData = papaparse.parse(matchesData, {
    header: true,
})

const deliveriesParsedData = papaparse.parse(deliveriesData, {
    header: true,
})

function strikeRateOfBatsmanEachSeason() {
    let years = matchesParsedData.data.reduce((accumulator, match) => {
        const season = match.season
        if (!accumulator.includes(season) && season != undefined) {
            accumulator.push(season)
        }
        return accumulator
    }, [])
    const matchIdForSeason = years.reduce((accumulator, year) => {
        const yearList = matchesParsedData.data.filter((match) => match['season'] === year)
            .map((match) => match['id'])
        accumulator[year] = yearList
        return accumulator
    }, {})
    const toatlRunsByBatsmanSeason = years.reduce((accumulator, year) => {
        accumulator[year] = deliveriesParsedData.data.reduce((result, eachDelivery) => {
            if (matchIdForSeason[year].includes(eachDelivery['match_id'])) {
                const batsmanName = eachDelivery['batsman']
                const batsmanRuns = parseInt(eachDelivery['batsman_runs'])
                const wideRuns = eachDelivery['wide_runs']
                const noballRuns = eachDelivery['noball_runs']
                if (result[batsmanName] == undefined) {

                    result[batsmanName] = { runs: 0, balls: 0, strikeRateBatsman: 0 }
                }
                result[batsmanName].runs += batsmanRuns
                if (wideRuns === '0' && noballRuns === '0') {
                    result[batsmanName].balls++

                }
                result[batsmanName].strikeRateBatsman = (result[batsmanName].runs / result[batsmanName].balls) * 100
            }

            return result

        }, {});
        return accumulator
    }, {});
    const strikeRateByBatsman = years.reduce((accumulator, year) => {
        accumulator[year] = {}
        Object.entries(toatlRunsByBatsmanSeason[year]).map(([batsmanName, batsmanData]) => {
            const { runs, balls, strikeRateBatsman } = batsmanData
            accumulator[year][batsmanName] = strikeRateBatsman
        })
        return accumulator
    }, {});
    return strikeRateByBatsman
}

module.exports = strikeRateOfBatsmanEachSeason