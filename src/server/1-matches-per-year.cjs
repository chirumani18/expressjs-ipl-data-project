const fs = require("fs")
const path = require("path")
const papaparse = require("papaparse")

const matchesFilePath = path.join(__dirname, "..", 'data/matches.csv')

let matchesData = fs.readFileSync(matchesFilePath, 'utf-8')

const parsedData = papaparse.parse(matchesData, {
    header: true,
})

function matchesPerYear(parsedData) {
    let matches = parsedData.data.map((match) => {
        return match.season
    })

    let matchesInYear = matches.reduce((accumulator, year) => {
        if (accumulator[year] === undefined) {
            accumulator[year] = 1
        }
        else {
            accumulator[year] += 1
        }
        return accumulator
    }, {})
    return matchesInYear
}

module.exports = matchesPerYear(parsedData)