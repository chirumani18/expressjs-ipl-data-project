const fs = require('fs')
const path = require("path")
const papaparse = require('papaparse')

const matchesFilePath = path.join(__dirname, "..", 'data/matches.csv')

let matchesData = fs.readFileSync(matchesFilePath, 'utf-8')

const parsedData = papaparse.parse(matchesData, {
    header: true,
})


function wonBothMatchesAndToss() {
    let wonTossAndMatches = parsedData.data.reduce((accumulator, match) => {
        if (match['toss_winner'] == match['winner']) {
            winner = match['winner'];
            if (accumulator[winner] == undefined) {
                accumulator[winner] = 1;
            }
            else {
                accumulator[winner] += 1;
            }
        }
        return accumulator;
    }, {})
    return wonTossAndMatches;
}
module.exports = wonBothMatchesAndToss
